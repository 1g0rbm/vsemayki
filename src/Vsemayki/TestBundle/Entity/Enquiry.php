<?php

namespace Vsemayki\TestBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Enquiry
{
    /**
     * Url-адрес для сокращения
     *
     * @Assert\Url()
     * @Assert\NotBlank()
     *
     * @var string $url
     */
    protected $url;

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }
}