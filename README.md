Тестовое задание
===================

**Развертка приложения: **  

* Склонировать репозиторий:

```
#!bash

git clone https://1g0rbm@bitbucket.org/1g0rbm/vsemayki.git

```  

* Установить необходимые зависимости: 

```

#!bash

composer install;
npm install;
bower install;

```  

* Создать базу данных:  

```

#!bash

php bin/console doctrine:database:create;
php bin/console doctrine:schema:create;

```  

* Настроить веб-сервер. Можно использовать стандартный конфиг из [документации](http://symfony.com/doc/current/setup/web_server_configuration.html)