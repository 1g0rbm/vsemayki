<?php

namespace Vsemayki\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Vsemayki\TestBundle\Entity\Enquiry;
use Vsemayki\TestBundle\Form\EnquireType;

class PageController extends Controller
{
    public function indexAction(Request $request)
    {
        $enquire = new Enquiry();
        $form = $this->createForm(EnquireType::class, $enquire);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            $response = [];
            if ($form->isValid()) {
                $response['status'] = 'OK';
                $response['message'] = '';
                $response['short_url'] = $this->get('vsemayki_test.shorter')->cut($enquire->getUrl());
            } else {
                $response['status'] = 'BAD';
                $response['message'] = 'Is not url';
            }

            return new JsonResponse($response);
        }

        return $this->render('VsemaykiTestBundle:Page:index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function redirectAction($url)
    {
        $matching = $this->getDoctrine()
            ->getRepository('VsemaykiTestBundle:Matching')
            ->findOneBy(['shortUrl' => 'http://' . $_SERVER['SERVER_NAME'] . '/' . $url]);

        if (!empty($matching)) {
            return $this->redirect($matching->getOriginUrl());
        }

        return $this->render('VsemaykiTestBundle:Page:error_redirect.html.twig');
    }
}