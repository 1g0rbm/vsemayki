<?php

namespace Vsemayki\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Matching
 *
 * @ORM\Entity
 * @ORM\Table(name="matching")
 *
 * @package Vsemayki\TestBundle\Entity
 */
class Matching
{
    /**
     * @ORM\id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $originUrl;

    /**
     * @ORM\Column(type="string")
     */
    protected $shortUrl;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOriginUrl()
    {
        return $this->originUrl;
    }

    /**
     * @param mixed $originUrl
     */
    public function setOriginUrl($originUrl)
    {
        $this->originUrl = $originUrl;
    }

    /**
     * @return mixed
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }

    /**
     * @param mixed $shortUrl
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }
}