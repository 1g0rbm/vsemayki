<?php

namespace Vsemayki\TestBundle\Entity\Repository;

use \Doctrine\ORM\EntityRepository;

class MatchingRepository extends EntityRepository
{
    public function getByShortUrl($shortUrl)
    {
        $query = $this->createQueryBuilder(
            'SELECT u
            FROM VsemaykiTestBundle:Matching m
            WHERE m.shortUrl
        ');
    }
}