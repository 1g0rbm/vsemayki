<?php

namespace Vsemayki\TestBundle\Services;

use \Doctrine\Bundle\DoctrineBundle\Registry;
use \Vsemayki\TestBundle\Entity\Matching;

class Shorter
{
    protected $em;
    protected $longUrl;
    protected $serverName;
    protected $string;

    public function __construct(Registry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->serverName = $_SERVER['SERVER_NAME'];
        $this->protocol = 'http://';
        $this->string = '1234567890qQwWeErRtTyYuUiIoOpPaAsSdDfFgGhHjJkKlLzZxXcCvVbBnNmM_-';
    }

    public function cut($url)
    {
        $this->longUrl = $url;

        $matching = $this->em->getRepository('VsemaykiTestBundle:Matching')->findOneBy(['originUrl' => $this->longUrl]);
        if (empty($matching)) {
            $matching = new Matching();
            $matching->setOriginUrl($this->longUrl);
            $matching->setShortUrl($this->makeShortUrl());
            $this->em->persist($matching);

            $this->em->flush();
        }

        return $matching->getShortUrl();
    }

    private function makeShortUrl()
    {
        return "{$this->protocol}{$this->serverName}/{$this->getCode()}";
    }

    private function getCode($length = 7)
    {
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $pos = mt_rand(0, strlen($this->string) - 1);
            $str .= $this->string[$pos];
        }

        return $str;
    }
}