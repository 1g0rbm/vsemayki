$('.btn.submit').click(function () {
    var url = $(this).attr('path'),
        data = $('.base-form').serialize();

    $('.notice').css({display: 'none'});
    $('.notice .msg').html('');

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: function (response) {

            console.log(response);

            if (response['status'] === 'OK') {
                $('.notice.success').css({display: 'block'});
                $('.notice.success .msg').append(response['short_url']);
            }

            if (response['status'] === 'BAD') {
                $('.notice.bad').css({display: 'block'});
                $('.notice.bad .msg').append(response['message']);
            }
        },
        error: function (jqXHR) {
            $('.notice.bad').css({display: 'block'});
            $('.notice.bad .msg').append('Неизвестная ошибка');
        }
    });

});